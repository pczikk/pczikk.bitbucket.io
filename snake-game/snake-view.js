
function generateGrid() {
    $("#gridParent").width(gridProps.cellSize*gridProps.columns).height(gridProps.cellSize*gridProps.rows);

    for(let i = 0; i < gridProps.columns; i++) {

        for(let j = 0; j < gridProps.rows; j++) {

            let newCell = $("<div class='cells'></div>"); 
            newCell.width(gridProps.cellSize).height(gridProps.cellSize).css("left", i*gridProps.cellSize).css("top", j*gridProps.cellSize).attr("data-row", j).attr("data-column", i);
            $("#gridParent").append(newCell);
        }
    }
}

function drawSnake() {
    $(".snake").removeClass("snake");
    $(".head").removeClass("head");
    
    snake.forEach(function (snakeCell, counter) {
        let bodyPart = $("[data-row = '" + snakeCell[1] + "'][data-column = '" + snakeCell[0] + "']");

        if (counter == 0) {
            bodyPart.addClass("head").addClass("snake");
        } else {
            bodyPart.addClass("snake");
        }
        //toto priradi class vsetkym polickam v mriezke, ktorych koord. sa nachadzaju v poli snake
    });
}

function drawFruits() {
    $("[data-row = '" + fruit[1] + "'][data-column = '" + fruit[0] + "']").addClass("fruit");
}

function removeFruits() {
    $("[data-row = '" + fruit[1] + "'][data-column = '" + fruit[0] + "']").removeClass("fruit");
}

function initHandlers() {
    $("#startSnake").click(startMoving);
    $("#stopSnake").click(stopMoving);
    $("#resetGame").click(resetGame);
}


function displayPoints() {
    $("#points").html("SCORE: " + points);
}

function setGameState(state) {
    switch(state) {
        case STATE.fresh:
            $("#resetGame").prop('disabled', true);
            $("#stopSnake").prop('disabled', true);
            $("#startSnake").prop('disabled', false);
            $("#gameOver").html("");
            $("#topScores-object").hide();
            break;

        case STATE.running:
            $("#resetGame").prop('disabled', true);
            $("#stopSnake").prop('disabled', false);
            $("#startSnake").prop('disabled', true);

            $(document).on('keydown.snake', function(e) {
                if (e.code === "ArrowUp") {
                    clickUp();
                }
                else if (e.code === "ArrowRight") {
                    clickRight();
                } 
                else if (e.code === "ArrowDown") {
                    clickDown();
                }
                else if (e.code === "ArrowLeft") {
                    clickLeft();
                }
            });

            $("#up").on('click', function() {
                lightUpArrow(this);
                clickUp();
            });
            $("#right").on('click', function() {
                lightUpArrow(this);
                clickRight();
            });
            $("#down").on('click', function() {
                lightUpArrow(this);
                clickDown();
            });
            $("#left").on('click', function() {
                lightUpArrow(this);
                clickLeft();
            });
            break;

        case STATE.paused:
            $("#resetGame").prop('disabled', false);
            $("#stopSnake").prop('disabled', true);
            $("#startSnake").prop('disabled', false);
            $("#up").off('click');
            $("#right").off('click');
            $("#down").off('click');
            $("#left").off('click');
            $(document).off('keydown.snake');
            break;

        case STATE.stuck:
            $("#resetGame").prop('disabled', false);
            $("#stopSnake").prop('disabled', true);
            $("#startSnake").prop('disabled', true);
            $("#up").off('click');
            $("#right").off('click');
            $("#down").off('click');
            $("#left").off('click');
            $(document).off('keydown.snake');
            $("#gameOver").html("GAME OVER!");
            $("#topScores-object").show();
            break;
    }
}

function lightUpArrow(clickedArrow) {
    $(clickedArrow).addClass("litUp");
    setTimeout(function() {
        $(clickedArrow).removeClass("litUp");
    }, 200);
}

function hideArrowsOnWeb() {
    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
        // true for mobile device
        return;
      }else{
        // false for mobile device
        $(".arrows").hide();
      }
}

function displayTopScores(topScores) {
    for(i = 0; i < 10; i++) {
        $('#topScores tr:eq(1)').remove();
    };
    
    topScores.forEach(function(scoreObject) {
        let player = scoreObject.player;
        let score = scoreObject.score;
        let time = scoreObject.time;
        $('#topScores').append(`<tr><td>${player}</td><td>${score}</td><td>${time}</td></tr>`);
    });
}