const scoreApiUrl = 'https://score-spark.herokuapp.com/api/scores/';
const gameTitle = 'snake-test-2';
const scoreLimit = 5;

function saveNewScore(player, score, callback) {
    let payloadData = {
        player: player,
        score: score
    };
    let payload = JSON.stringify(payloadData);
    $.ajax({
        url: scoreApiUrl + gameTitle,
        type: 'post',
        contentType: 'application/json',
        headers: {
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        data: payload,
        success: function(data) {
            callback(data._id);
        },
        error: function(e) {
            console.log("Error saving new score:" + e);
        }
    });


}



function getTopScores(callback) {
    // let topScores = getmockedScores();

    $.ajax({
        url: scoreApiUrl + gameTitle + '?limit=' + scoreLimit,
        type: 'get',
        contentType: 'application/json',
        headers: {
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        success: function(data) {
            let topScores = [];
            data.forEach(function(item) {
                let score = {
                    id: item._id,
                    player: item.player,
                    score: item.score,
                    time: item.time
                };
                topScores.push(score);
            });
            callback(topScores);
        },
        error: function(e) {
            console.log("Error retrieving top scores:" + e);
        }
    });
}

/*
function getmockedScores() {
    let topScores = [];
    let score1 = {
        _id: "603abcf2e82e32b27b743c48",
        player: "petko",
        score: 52,
        timestamp: 1614462194162,
        ipAddress: "102.10.23.23",
        userAgent: "PostmanRuntime/7.26.8",
        time: "27 Feb 2021, 21:43"
    };
    topScores.push(score1);
    let score2 = {
        _id: "603abcf2e82e32b27b743c49",
        player: "kubko",
        score: 30,
        timestamp: 1614462194161,
        ipAddress: "102.10.23.23",
        userAgent: "PostmanRuntime/7.26.8",
        time: "22 Jan 2021, 15:43"
    };
    topScores.push(score2);
    return topScores;
}
*/