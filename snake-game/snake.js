
let snake = [];
let fruit = [];
let timeoutId;
let direction;
let points = 0;
let playerName;
const SPEED_INCREASE = 50;
const FINAL_GAMESPEED = 50;
const INITIAL_SPEED = 500;

let gameSpeed = INITIAL_SPEED; 

const gridProps = {
    rows: 25,
    columns: 25,
    cellSize: 25
};

const DIRECTION = {
    up: 0,
    right: 1,
    down: 2,
    left: 3
};

const STATE = {
    fresh: 1,
    running: 2,
    paused: 3, 
    stuck: 4
};


function generateSnake() {
    snake = [[2,8], [2,9], [2,10], [2,11], [2,12], [2,13], [2,14], [2,15]];
    direction = DIRECTION.up;
    drawSnake();
}

function generateFruits() {
    let fruitConflict;
    do {
        fruitConflict = false;
        fruit = [Math.floor(Math.random() *gridProps.columns), Math.floor(Math.random() *gridProps.rows)];

        snake.forEach(function(snakeCell) {
            if((snakeCell[0] == fruit[0]) && (snakeCell[1] == fruit[1])) {
                fruitConflict = true;
            }
        });
    } while(fruitConflict);

    drawFruits();
}


function moveSnake() {
    let newHeadConflict;
    let newSnakeHead = Array.from(snake[0]);
        switch(direction) {
            case DIRECTION.up:
                newSnakeHead[1]--;
                break;
            case DIRECTION.right:
                newSnakeHead[0]++;
                break;
            case DIRECTION.down:
                newSnakeHead[1]++;
                break;
            case DIRECTION.left:
                newSnakeHead[0]--;
                break;
        }

        newHeadConflict = false;
        snake.forEach(function(snakeCell) {
            if((snakeCell[0] == newSnakeHead[0]) && (snakeCell[1] == newSnakeHead[1])) {
                newHeadConflict = true;
            }
        });

        if((newSnakeHead[0] < 0) || (newSnakeHead[1] < 0) || (newSnakeHead[0] > gridProps.columns - 1) || (newSnakeHead[1] > gridProps.rows - 1)) {
            newHeadConflict = true;
        }

        if(newHeadConflict) {
            clearTimeout(timeoutId);
            gameOver();
            return;
        }
    
    snake.unshift(newSnakeHead);

    let isThereFruit = checkAndRemoveFruit(newSnakeHead);
    if(isThereFruit) {
        gameSpeed = (gameSpeed>FINAL_GAMESPEED)?(gameSpeed - SPEED_INCREASE):gameSpeed;
        generateFruits();
    } else {
        snake.pop();
    }

    drawSnake();
    timeoutId = setTimeout(moveSnake, gameSpeed);
}

function checkAndRemoveFruit(newSnakeHead) {
    if((newSnakeHead[0] == fruit[0]) && (newSnakeHead[1] == fruit[1])) {
        points++;
        displayPoints();
        removeFruits();
        fruit = [];
        return true;
    } else {
        return false;
    }
}


function clickUp() {
    if(direction == DIRECTION.down) {
        return;
    }
    direction = DIRECTION.up;
    clearTimeout(timeoutId);
    moveSnake();
}

function clickRight() {
    if(direction == DIRECTION.left) {
        return;
    }
    direction = DIRECTION.right;
    clearTimeout(timeoutId);
    moveSnake();
}

function clickDown() {
    if(direction == DIRECTION.up) {
        return;
    }
    direction = DIRECTION.down;
    clearTimeout(timeoutId);
    moveSnake();
}

function clickLeft() {
    if(direction == DIRECTION.right) {
        return;
    }
    direction = DIRECTION.left;
    clearTimeout(timeoutId);
    moveSnake();
}


function startMoving() {
    setGameState(STATE.running);
    moveSnake();
}

function stopMoving() {
    setGameState(STATE.paused);
    clearTimeout(timeoutId);
}

function resetGame() {
    setGameState(STATE.fresh);
    generateSnake();
    gameSpeed = INITIAL_SPEED;
    points = 0;
    displayPoints();
}

function gameOver() {
    setGameState(STATE.stuck);
    saveNewScore(playerName, points, function (scoreId){
        console.log(`New score id = ${scoreId}`);
        getTopScores(displayTopScores);
    })
}



function checkForPlayerInLocalStorege() {
    playerName = localStorage.getItem('playerName');

    if(!playerName) {
        window.location.replace("./register.html");
    } else {
        $('#player-name-value').text(playerName);
    }
}


function onStartProgram() {
    checkForPlayerInLocalStorege();
    hideArrowsOnWeb();
    generateGrid();
    generateSnake();
    displayPoints();
    initHandlers();
    generateFruits();
    setGameState(STATE.fresh);
}









/*function moveSnakeRandom() {
    let newHeadConflict;
    let newSnakeHead;
    let attempt = 0;
    const MAX_WRONG_ATTEMPTS = 30;

    do {
        if(attempt++ == MAX_WRONG_ATTEMPTS) {
            console.log("Master, plese help, I'm stuck!");
            clearInterval(intervalId);
            setGameState(STATE.stuck);
            return;
        }

        let direction = Math.floor(Math.random() *4);
        newSnakeHead = Array.from(snake[0]);

        switch(direction) {
            case DIRECTION.up:
                newSnakeHead[1]--;
                break;
            case DIRECTION.right:
                newSnakeHead[0]++;
                break;
            case DIRECTION.down:
                newSnakeHead[1]++;
                break;
            case DIRECTION.left:
                newSnakeHead[0]--;
                break;
        }
        newHeadConflict = false;
        snake.forEach(function(snakeCell) {
            if((snakeCell[0] == newSnakeHead[0]) && (snakeCell[1] == newSnakeHead[1])) {
                newHeadConflict = true;
                console.log("vymyslel som smer kam nemozem ist, som tam ja, lebo tam mam telo!");
            }
        });

        if((newSnakeHead[0] < 0) || (newSnakeHead[1] < 0) || (newSnakeHead[0] > gridProps.columns - 1) || (newSnakeHead[1] > gridProps.rows - 1)) {
            newHeadConflict = true;
            console.log("vymyslel som smer kam nemozem ist, lebo je tam hranic!");
        }

    } while (newHeadConflict);

    snake.unshift(newSnakeHead);
    snake.pop();

    drawSnake();
}
*/
